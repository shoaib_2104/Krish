//
//  Constants.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import Foundation
import Foundation
import UIKit

var buttonCornerRadius:CGFloat = 10
let QuickBloxuserpassword = ""
let AppTitleForNotificationBanner = "Krish Technolabs"
let errorViewFixHeight:CGFloat = 30
let menuSelectedCell = "menuSelectedCell"
let PasswordLengthMin = 6
let ErrorInternetConnection = "Please check your internet connectivity."
let NoInternetConnection = "There is no internet connection."
let displayDateFormate = "dd/MM/yyyy"
let displayTimeFormate = "HH:mm"
let sendDateFormate = "yyyy-MM-dd HH:mm:ss"
let sendBirthDayDateFormate = "yyyy-MM-dd"
let currencyValue = "£"
let textFeildFontSize:CGFloat = 18
