
//
//  SignInViewController.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//


import UIKit
import JVFloatLabeledTextField
import Reachability

@available(iOS 13.0, *)
class SignInViewController: UIViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var txtEmailAddress: JVFloatLabeledTextField!
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailAddressTop: NSLayoutConstraint!
    @IBOutlet weak var passwordTop: NSLayoutConstraint!
    @IBOutlet weak var errorViewEmail: ErrorView!
    @IBOutlet weak var errorHeightEmail: NSLayoutConstraint!
    @IBOutlet weak var errorViewPassword: ErrorView!
    @IBOutlet weak var errorHeightPassword: NSLayoutConstraint!
    
    var scrollPosition = 0
    var reachability = Reachability()
    let url:String = "https://mcstaging.tamata.com/rest/V1/integration/customer/token"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitial()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setUpInitial() {
        btnLogin.layer.cornerRadius = buttonCornerRadius
        btnLogin.clipsToBounds = true
        
        txtPassword.delegate = self
        txtPassword.autocorrectionType = .no
        txtEmailAddress.delegate = self
        txtEmailAddress.autocorrectionType = .no
        
        txtEmailAddress.superview?.layer.borderColor = AppColor.errorColor.cgColor
        txtPassword.superview?.layer.borderColor = AppColor.errorColor.cgColor
        
        txtEmailAddress.font = appFont.shared.customRegularSize(textFeildFontSize)
        txtPassword.font = appFont.shared.customRegularSize(textFeildFontSize)
        
        txtEmailAddress.placeholderColor = AppColor.textPlaceHolderColor
        txtPassword.placeholderColor = AppColor.textPlaceHolderColor
        
        txtEmailAddress.floatingLabelTextColor = AppColor.textPlaceLableColor
        txtEmailAddress.floatingLabelActiveTextColor = AppColor.textPlaceLableColor
        txtPassword.floatingLabelTextColor = AppColor.textPlaceLableColor
        txtPassword.floatingLabelActiveTextColor = AppColor.textPlaceLableColor
        
        errorViewEmail.isHidden = true
        errorViewEmail.backgroundColor = UIColor.clear
        errorHeightEmail.constant = 0
        
        errorViewPassword.isHidden = true
        errorViewPassword.backgroundColor = UIColor.clear
        errorHeightPassword.constant = 0
    }
    
    @IBAction func onClickForgotPassword(_ sender: UIButton) {
        
    }
    
    @IBAction func onClickLogin(_ sender: UIButton) {
        if validation() {
            if reachability?.connection == .wifi || reachability?.connection == .cellular {
                self.ShowProgressHUD()
                let strEmail = txtEmailAddress.text?.TrimString()
                let strPassword = txtPassword.text?.TrimString()
                let parameter = ["username":strEmail,"password":strPassword]
                APIServices.postUrlSessionWithJSONParams(urlString: url, params: parameter as [String : AnyObject]) { (response) in
                    if response.json.exists() {
                        let product = HomeViewController.storyboardInstance()!
                        self.navigationController?.pushViewController(product, animated: false)
                    }
                }
            } else {
                BaseClass.sharedInstance.showNotificationBanner(
                    AppTitleForNotificationBanner,
                    ErrorInternetConnection,
                    .Error
                )
            }
        }
    }
    
    @IBAction func onClickSignUp(_ sender: UIButton) {
    }
    
    @IBAction func onClickShowPassword(_ sender: UIButton) {
        if txtPassword.isSecureTextEntry == true {
            txtPassword.isSecureTextEntry = false
            btnShowPassword.setImage(#imageLiteral(resourceName: "ic_eye_off"), for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            btnShowPassword.setImage(#imageLiteral(resourceName: "ic_eye_on"), for: .normal)
        }
    }
    
    func validation() -> Bool {
        var checkValidation = true
        let strEmail = txtEmailAddress.text?.TrimString()
        let strPassword = txtPassword.text?.TrimString()
        scrollPosition = 0
        if strEmail == "" {
            checkValidation = false
            txtEmailAddress.superview?.layer.borderWidth = 1
            errorViewEmail.isHidden = false
            errorViewEmail.message = "Please enter email address."
            scrollPosition = scrollPosition == 0 ? 2 : scrollPosition
        } else if strEmail?.isValidEmail() == false {
            checkValidation = false
            txtEmailAddress.superview?.layer.borderWidth = 1
            errorViewEmail.isHidden = false
            errorViewEmail.message = "Please enter valid email address."
            scrollPosition = scrollPosition == 0 ? 2 : scrollPosition
        }
        
        if strPassword == "" {
            checkValidation = false
            txtPassword.superview?.layer.borderWidth = 1
            errorViewPassword.isHidden = false
            errorViewPassword.message = "Please enter password."
            scrollPosition = scrollPosition == 0 ? 4 : scrollPosition
        } else if (strPassword?.count)! < PasswordLengthMin {
            checkValidation = false
            txtPassword.superview?.layer.borderWidth = 1
            errorViewPassword.isHidden = false
            errorViewPassword.message = "Please enter password at least 6 character."
            scrollPosition = scrollPosition == 0 ? 4 : scrollPosition
        }
        
        if checkValidation == false {
            self.perform(#selector(setScrollViewContentSize), with: nil, afterDelay: 0.1)
            self.perform(#selector(setScrollViewPosition), with: nil, afterDelay: 0.1)
        }
        return checkValidation
    }
    
    @objc func setScrollViewPosition() {
        var diff = scrollView.contentSize.height - scrollView.frame.height
        if diff < 0 {
            diff = 0
        }
        if scrollPosition == 1 {
            let yValue = (txtEmailAddress.superview?.frame.origin.y)!
            if yValue + scrollView.frame.height < scrollView.contentSize.height {
                scrollView.setContentOffset(CGPoint.init(x: 0, y: (txtEmailAddress.superview?.frame.origin.y)!), animated: false)
            } else {
                scrollView.setContentOffset(CGPoint.init(x: 0, y: diff), animated: false)
            }
        } else if scrollPosition == 2 {
            let yValue = (txtPassword.superview?.frame.origin.y)!
            if yValue + scrollView.frame.height < scrollView.contentSize.height {
                scrollView.setContentOffset(CGPoint.init(x: 0, y: (txtPassword.superview?.frame.origin.y)!), animated: false)
            } else {
                scrollView.setContentOffset(CGPoint.init(x: 0, y: diff), animated: false)
            }
        }
    }
    
    @objc func setScrollViewContentSize() {
        scrollView.contentSize = CGSize.init(width: self.view.frame.width, height: btnSignUp.frame.origin.y + btnSignUp.frame.height + 15)
        btnSignUp.superview?.translatesAutoresizingMaskIntoConstraints = true
        btnSignUp.superview?.frame = CGRect.init(x: (btnSignUp.superview?.frame.origin.x)!, y: (btnSignUp.superview?.frame.origin.y)!, width: self.view.frame.size.width, height: scrollView.contentSize.height)
    }
}
@available(iOS 13.0, *)
extension SignInViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmailAddress {
            textField.superview?.layer.borderWidth = 0
            if errorHeightEmail.constant > 0 {
                errorViewEmail.isHidden = true
                self.perform(#selector(setScrollViewContentSize), with: nil, afterDelay: 0.1)
            }
            errorHeightEmail.constant = 0
        }
        
        if textField == txtPassword {
            textField.superview?.layer.borderWidth = 0
            if errorHeightPassword.constant > 0 {
                errorViewPassword.isHidden = true
                self.perform(#selector(setScrollViewContentSize), with: nil, afterDelay: 0.1)
            }
            errorHeightPassword.constant = 0
        }
        return true
    }
}
