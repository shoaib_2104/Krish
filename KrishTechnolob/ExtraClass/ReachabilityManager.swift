//
//  ReachabilityManager.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit
import Reachability

@objc protocol ReachableDelegate:class {
    func reachabilityChange()
}

class ReachabilityManager: NSObject {

    static let shared = ReachabilityManager()  // 2. Shared instance
    
    // 3. Boolean to track network reachability
    var isNetworkAvailable : Bool {
        let reachability = Reachability()!
        return (reachability.connection == .wifi || reachability.connection == .cellular) ? true : false
    }
    
    // 5. Reachibility instance for Network status monitoring
    let reachability = Reachability()!
    
    weak var delegate:ReachableDelegate?
    /// Called whenever there is a change in NetworkReachibility Status
    ///
    /// — parameter notification: Notification with the Reachability instance
    @objc func reachabilityChanged(notification: Notification) {
        delegate?.reachabilityChange()
    }
    
    /// Starts monitoring the network availability status
    func startMonitoring() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: nil)
        do{
            try reachability.startNotifier()
        }catch{
            debugPrint("Could not start reachability notifier")
        }
    }
    
    /// Stops monitoring the network availability status
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged,
                                                  object: nil)
    }
}
