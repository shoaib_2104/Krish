//
//  ListViewController.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Reachability
import MBProgressHUD

class ListViewController: UIViewController {
    
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var totalPages : Int = 10
    var productMdl : ProductModel!
    var productItems = [ProductItem]()
    var reachability = Reachability()
    
    static func storyboardInstance() -> ListViewController? {
        let Storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return Storyboard.instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
    }
    
    @IBOutlet weak var listTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listTableView.delegate = self
        listTableView.dataSource = self
        self.getProductsList(isFirst: true, currentPage: self.currentPage)
        
    }
    
    func getProductsList(isFirst:Bool, currentPage:Int){
        let url:String = "https://mcstaging.tamata.com/rest/V1/mdcategories/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=467&searchCriteria[filter_groups][0][filters][0][condition_type]=in&storeId=2&currencyCode=IQD&searchCriteria[pageSize]=10&categoryId=26&searchCriteria[currentPage]=\(currentPage)"
        if reachability?.connection == .wifi || reachability?.connection == .cellular {
            self.ShowProgressHUD()
            APIServices.gertUrlSession2(urlString: url, params: [:]) { (response) in
                if response.json.exists() {
                    self.productMdl = ProductModel.init(fromJson: response.json)
                    
                    if isFirst{
                        self.productItems = self.productMdl.items
                    }else{
                        self.productItems += self.productMdl.items
                    }
                    self.listTableView.reloadData()
                }
            }
        } else {
            BaseClass.sharedInstance.showNotificationBanner(
                AppTitleForNotificationBanner,
                ErrorInternetConnection,
                .Error
            )
        }
       self.HideProgressHUD()
    }
}
extension ListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listTableView.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath) as! ListTableCell
        if indexPath.row < productItems.count {
            cell.name.text = self.productItems[indexPath.row].name
            let price = "\(self.productItems[indexPath.row].price ?? 10)"
            cell.productPrice.text = price
            let apdURL = self.productItems[indexPath.row].extensionAttributes.appendedData.mediaPath ?? ""
            if self.productItems[indexPath.row].mediaGalleryEntries.count > 0{
                let imgURl = self.productItems[indexPath.row].mediaGalleryEntries[0].file ?? ""
                print(imgURl)
                let fullUrl = apdURL+imgURl
                cell.productImage.sd_setImage(with: URL(string: fullUrl), placeholderImage: #imageLiteral(resourceName: "krish"))
            }else{
                cell.productImage.image = #imageLiteral(resourceName: "krish")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.currentPage < self.totalPages{
            if indexPath.row == (self.productItems.count-1){
                //Call API
                self.currentPage = self.currentPage + 1
                self.getProductsList(isFirst: false, currentPage: self.currentPage)
            }
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: listTableView.bounds.width, height: CGFloat(44))

            self.listTableView.tableFooterView = spinner
            self.listTableView.tableFooterView?.isHidden = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150;
    }
}
