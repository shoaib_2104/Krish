//
//  ProductSearchCriterion.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductSearchCriterion : NSObject, NSCoding{

    var currentPage : Int!
    var filterGroups : [ProductFilterGroup]!
    var pageSize : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        currentPage = json["current_page"].intValue
        filterGroups = [ProductFilterGroup]()
        let filterGroupsArray = json["filter_groups"].arrayValue
        for filterGroupsJson in filterGroupsArray{
            let value = ProductFilterGroup(fromJson: filterGroupsJson)
            filterGroups.append(value)
        }
        pageSize = json["page_size"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if currentPage != nil{
        	dictionary["current_page"] = currentPage
        }
        if filterGroups != nil{
        var dictionaryElements = [[String:Any]]()
        for filterGroupsElement in filterGroups {
        	dictionaryElements.append(filterGroupsElement.toDictionary())
        }
        dictionary["filterGroups"] = dictionaryElements
        }
        if pageSize != nil{
        	dictionary["page_size"] = pageSize
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		currentPage = aDecoder.decodeObject(forKey: "current_page") as? Int
		filterGroups = aDecoder.decodeObject(forKey: "filter_groups") as? [ProductFilterGroup]
		pageSize = aDecoder.decodeObject(forKey: "page_size") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if currentPage != nil{
			aCoder.encode(currentPage, forKey: "current_page")
		}
		if filterGroups != nil{
			aCoder.encode(filterGroups, forKey: "filter_groups")
		}
		if pageSize != nil{
			aCoder.encode(pageSize, forKey: "page_size")
		}

	}

}
