//
//  ProductDefaultVendorDatum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductDefaultVendorDatum : NSObject, NSCoding{

    var approvedAt : String!
    var businessName : String!
    var condition : String!
    var conditionNote : AnyObject!
    var email : String!
    var externalId : AnyObject!
    var isDeleted : String!
    var isOffered : String!
    var logo : String!
    var marketplaceProductId : String!
    var parentId : AnyObject!
    var price : String!
    var productName : String!
    var productUrl : AnyObject!
    var qty : String!
    var ratingAvg : AnyObject!
    var reorderLevel : String!
    var showMicrosite : Bool!
    var sku : String!
    var specialFromDate : AnyObject!
    var specialPrice : String!
    var specialToDate : AnyObject!
    var status : String!
    var storeId : String!
    var stores : String!
    var totalSelling : AnyObject!
    var typeId : String!
    var vendorId : String!
    var vendorName : String!
    var vendorProductId : String!
    var vendorSku : String!
    var warrantyDescription : AnyObject!
    var warrantyType : AnyObject!
    var websiteId : String!
    var websites : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        approvedAt = json["approved_at"].stringValue
        businessName = json["business_name"].stringValue
        condition = json["condition"].stringValue
        conditionNote = json["condition_note"] as AnyObject
        email = json["email"].stringValue
        externalId = json["external_id"] as AnyObject
        isDeleted = json["is_deleted"].stringValue
        isOffered = json["is_offered"].stringValue
        logo = json["logo"].stringValue
        marketplaceProductId = json["marketplace_product_id"].stringValue
        parentId = json["parent_id"] as AnyObject
        price = json["price"].stringValue
        productName = json["product_name"].stringValue
        productUrl = json["product_url"] as AnyObject
        qty = json["qty"].stringValue
        ratingAvg = json["rating_avg"] as AnyObject
        reorderLevel = json["reorder_level"].stringValue
        showMicrosite = json["show_microsite"].boolValue
        sku = json["sku"].stringValue
        specialFromDate = json["special_from_date"] as AnyObject
        specialPrice = json["special_price"].stringValue
        specialToDate = json["special_to_date"] as AnyObject
        status = json["status"].stringValue
        storeId = json["store_id"].stringValue
        stores = json["stores"].stringValue
        totalSelling = json["total_selling"] as AnyObject
        typeId = json["type_id"].stringValue
        vendorId = json["vendor_id"].stringValue
        vendorName = json["vendor_name"].stringValue
        vendorProductId = json["vendor_product_id"].stringValue
        vendorSku = json["vendor_sku"].stringValue
        warrantyDescription = json["warranty_description"] as AnyObject
        warrantyType = json["warranty_type"] as AnyObject
        websiteId = json["website_id"].stringValue
        websites = json["websites"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if approvedAt != nil{
        	dictionary["approved_at"] = approvedAt
        }
        if businessName != nil{
        	dictionary["business_name"] = businessName
        }
        if condition != nil{
        	dictionary["condition"] = condition
        }
        if conditionNote != nil{
        	dictionary["condition_note"] = conditionNote
        }
        if email != nil{
        	dictionary["email"] = email
        }
        if externalId != nil{
        	dictionary["external_id"] = externalId
        }
        if isDeleted != nil{
        	dictionary["is_deleted"] = isDeleted
        }
        if isOffered != nil{
        	dictionary["is_offered"] = isOffered
        }
        if logo != nil{
        	dictionary["logo"] = logo
        }
        if marketplaceProductId != nil{
        	dictionary["marketplace_product_id"] = marketplaceProductId
        }
        if parentId != nil{
        	dictionary["parent_id"] = parentId
        }
        if price != nil{
        	dictionary["price"] = price
        }
        if productName != nil{
        	dictionary["product_name"] = productName
        }
        if productUrl != nil{
        	dictionary["product_url"] = productUrl
        }
        if qty != nil{
        	dictionary["qty"] = qty
        }
        if ratingAvg != nil{
        	dictionary["rating_avg"] = ratingAvg
        }
        if reorderLevel != nil{
        	dictionary["reorder_level"] = reorderLevel
        }
        if showMicrosite != nil{
        	dictionary["show_microsite"] = showMicrosite
        }
        if sku != nil{
        	dictionary["sku"] = sku
        }
        if specialFromDate != nil{
        	dictionary["special_from_date"] = specialFromDate
        }
        if specialPrice != nil{
        	dictionary["special_price"] = specialPrice
        }
        if specialToDate != nil{
        	dictionary["special_to_date"] = specialToDate
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if storeId != nil{
        	dictionary["store_id"] = storeId
        }
        if stores != nil{
        	dictionary["stores"] = stores
        }
        if totalSelling != nil{
        	dictionary["total_selling"] = totalSelling
        }
        if typeId != nil{
        	dictionary["type_id"] = typeId
        }
        if vendorId != nil{
        	dictionary["vendor_id"] = vendorId
        }
        if vendorName != nil{
        	dictionary["vendor_name"] = vendorName
        }
        if vendorProductId != nil{
        	dictionary["vendor_product_id"] = vendorProductId
        }
        if vendorSku != nil{
        	dictionary["vendor_sku"] = vendorSku
        }
        if warrantyDescription != nil{
        	dictionary["warranty_description"] = warrantyDescription
        }
        if warrantyType != nil{
        	dictionary["warranty_type"] = warrantyType
        }
        if websiteId != nil{
        	dictionary["website_id"] = websiteId
        }
        if websites != nil{
        	dictionary["websites"] = websites
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		approvedAt = aDecoder.decodeObject(forKey: "approved_at") as? String
		businessName = aDecoder.decodeObject(forKey: "business_name") as? String
		condition = aDecoder.decodeObject(forKey: "condition") as? String
		conditionNote = aDecoder.decodeObject(forKey: "condition_note") as? AnyObject
		email = aDecoder.decodeObject(forKey: "email") as? String
		externalId = aDecoder.decodeObject(forKey: "external_id") as? AnyObject
		isDeleted = aDecoder.decodeObject(forKey: "is_deleted") as? String
		isOffered = aDecoder.decodeObject(forKey: "is_offered") as? String
		logo = aDecoder.decodeObject(forKey: "logo") as? String
		marketplaceProductId = aDecoder.decodeObject(forKey: "marketplace_product_id") as? String
		parentId = aDecoder.decodeObject(forKey: "parent_id") as? AnyObject
		price = aDecoder.decodeObject(forKey: "price") as? String
		productName = aDecoder.decodeObject(forKey: "product_name") as? String
		productUrl = aDecoder.decodeObject(forKey: "product_url") as? AnyObject
		qty = aDecoder.decodeObject(forKey: "qty") as? String
		ratingAvg = aDecoder.decodeObject(forKey: "rating_avg") as? AnyObject
		reorderLevel = aDecoder.decodeObject(forKey: "reorder_level") as? String
		showMicrosite = aDecoder.decodeObject(forKey: "show_microsite") as? Bool
		sku = aDecoder.decodeObject(forKey: "sku") as? String
		specialFromDate = aDecoder.decodeObject(forKey: "special_from_date") as? AnyObject
		specialPrice = aDecoder.decodeObject(forKey: "special_price") as? String
		specialToDate = aDecoder.decodeObject(forKey: "special_to_date") as? AnyObject
		status = aDecoder.decodeObject(forKey: "status") as? String
		storeId = aDecoder.decodeObject(forKey: "store_id") as? String
		stores = aDecoder.decodeObject(forKey: "stores") as? String
		totalSelling = aDecoder.decodeObject(forKey: "total_selling") as? AnyObject
		typeId = aDecoder.decodeObject(forKey: "type_id") as? String
		vendorId = aDecoder.decodeObject(forKey: "vendor_id") as? String
		vendorName = aDecoder.decodeObject(forKey: "vendor_name") as? String
		vendorProductId = aDecoder.decodeObject(forKey: "vendor_product_id") as? String
		vendorSku = aDecoder.decodeObject(forKey: "vendor_sku") as? String
		warrantyDescription = aDecoder.decodeObject(forKey: "warranty_description") as? AnyObject
		warrantyType = aDecoder.decodeObject(forKey: "warranty_type") as? AnyObject
		websiteId = aDecoder.decodeObject(forKey: "website_id") as? String
		websites = aDecoder.decodeObject(forKey: "websites") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if approvedAt != nil{
			aCoder.encode(approvedAt, forKey: "approved_at")
		}
		if businessName != nil{
			aCoder.encode(businessName, forKey: "business_name")
		}
		if condition != nil{
			aCoder.encode(condition, forKey: "condition")
		}
		if conditionNote != nil{
			aCoder.encode(conditionNote, forKey: "condition_note")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if externalId != nil{
			aCoder.encode(externalId, forKey: "external_id")
		}
		if isDeleted != nil{
			aCoder.encode(isDeleted, forKey: "is_deleted")
		}
		if isOffered != nil{
			aCoder.encode(isOffered, forKey: "is_offered")
		}
		if logo != nil{
			aCoder.encode(logo, forKey: "logo")
		}
		if marketplaceProductId != nil{
			aCoder.encode(marketplaceProductId, forKey: "marketplace_product_id")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if productUrl != nil{
			aCoder.encode(productUrl, forKey: "product_url")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if ratingAvg != nil{
			aCoder.encode(ratingAvg, forKey: "rating_avg")
		}
		if reorderLevel != nil{
			aCoder.encode(reorderLevel, forKey: "reorder_level")
		}
		if showMicrosite != nil{
			aCoder.encode(showMicrosite, forKey: "show_microsite")
		}
		if sku != nil{
			aCoder.encode(sku, forKey: "sku")
		}
		if specialFromDate != nil{
			aCoder.encode(specialFromDate, forKey: "special_from_date")
		}
		if specialPrice != nil{
			aCoder.encode(specialPrice, forKey: "special_price")
		}
		if specialToDate != nil{
			aCoder.encode(specialToDate, forKey: "special_to_date")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if storeId != nil{
			aCoder.encode(storeId, forKey: "store_id")
		}
		if stores != nil{
			aCoder.encode(stores, forKey: "stores")
		}
		if totalSelling != nil{
			aCoder.encode(totalSelling, forKey: "total_selling")
		}
		if typeId != nil{
			aCoder.encode(typeId, forKey: "type_id")
		}
		if vendorId != nil{
			aCoder.encode(vendorId, forKey: "vendor_id")
		}
		if vendorName != nil{
			aCoder.encode(vendorName, forKey: "vendor_name")
		}
		if vendorProductId != nil{
			aCoder.encode(vendorProductId, forKey: "vendor_product_id")
		}
		if vendorSku != nil{
			aCoder.encode(vendorSku, forKey: "vendor_sku")
		}
		if warrantyDescription != nil{
			aCoder.encode(warrantyDescription, forKey: "warranty_description")
		}
		if warrantyType != nil{
			aCoder.encode(warrantyType, forKey: "warranty_type")
		}
		if websiteId != nil{
			aCoder.encode(websiteId, forKey: "website_id")
		}
		if websites != nil{
			aCoder.encode(websites, forKey: "websites")
		}

	}

}
