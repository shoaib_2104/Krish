//
//  ProductFilter.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductFilter : NSObject, NSCoding{

    var code : String!
    var label : String!
    var options : [ProductOption]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        code = json["code"].stringValue
        label = json["label"].stringValue
        options = [ProductOption]()
        let optionsArray = json["options"].arrayValue
        for optionsJson in optionsArray{
            let value = ProductOption(fromJson: optionsJson)
            options.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if code != nil{
        	dictionary["code"] = code
        }
        if label != nil{
        	dictionary["label"] = label
        }
        if options != nil{
        var dictionaryElements = [[String:Any]]()
        for optionsElement in options {
        	dictionaryElements.append(optionsElement.toDictionary())
        }
        dictionary["options"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		code = aDecoder.decodeObject(forKey: "code") as? String
		label = aDecoder.decodeObject(forKey: "label") as? String
		options = aDecoder.decodeObject(forKey: "options") as? [ProductOption]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if label != nil{
			aCoder.encode(label, forKey: "label")
		}
		if options != nil{
			aCoder.encode(options, forKey: "options")
		}

	}

}
