//
//  APIService.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

public typealias JSONDictionary = [String: AnyObject]

class APIServices: NSObject {
    // MARK:- Get Api hitting Model
    class func getUrlSession(urlString: String, params: [String : AnyObject]? ,header : [String : String] ,  completion completionHandler:@escaping (_ response: DataResponse<Any>) -> ()) {
        Alamofire.request(urlString,method: .get, parameters: params, encoding : JSONEncoding.default , headers: header).responseJSON { (response) in
            print(response.json)
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    completionHandler(response)
                }else{
                    completionHandler(response)
                }
                break
            case .failure(_):
                //ez.topMostVC?.view.makeToast((response.error?.localizedDescription)!)
                completionHandler(response)
                break
            }
        }
    }
    
    // MARK: API with send json params
    class func gertUrlSession2(urlString: String, params: Parameters  ,  completion completionHandler:@escaping (_ response: DataResponse<Any>) -> ()) {
        
        Alamofire.request(urlString).responseJSON { (response) in
            print(response.json)
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    completionHandler(response)
                }else{
                    completionHandler(response)
                }
                break
            case .failure(_):
                //ez.topMostVC?.view.makeToast((response.error?.localizedDescription)!)
                completionHandler(response)
                break
            }
        }
    }
    // MARK: API with send json params
    class func postUrlSessionWithJSONParams(urlString: String, params: Parameters  ,  completion completionHandler:@escaping (_ response: DataResponse<Any>) -> ()) {
        Alamofire.request(urlString,method: .post, parameters: params, encoding : JSONEncoding.default ).responseJSON { (response) in
            print(response.json)
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    completionHandler(response)
                }else{
                    completionHandler(response)
                }
                break
            case .failure(_):
                completionHandler(response)
                break
            }
        }
    }
}
extension DataResponse{
    var json:JSON {
        return JSON(self.result.value as Any)
    }
    var dictionary:[String:AnyObject]?{
        return try! JSONSerialization.jsonObject(with: self.result.value as! Data, options: .init(rawValue: 0)) as? [String:AnyObject]
    }
    var userDetails:[String:AnyObject]?{
        return dictionary?["userDetails"] as? [String:AnyObject]
    }
    var dictionaryFromJson:[String:AnyObject]?{
        return self.result.value as? [String:AnyObject]
    }
    var userDetailsFromJson:[String:AnyObject]?{
        return dictionaryFromJson?["userDetails"] as? [String:AnyObject]
    }
}
