//
//  AllTheExtension.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import Reachability

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneXR = "iPhone XR"
        case iPhoneX_iPhoneXS = "iPhone X,iPhoneXS"
        case iPhoneXSMax = "iPhoneXS Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhoneXR
        case 1624:
            return .iPhoneXR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX_iPhoneXS
        case 2688:
            return .iPhoneXSMax
        default:
            return .unknown
        }
    }
}

extension String {
    
    public func TrimString() -> String {
        
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
    
    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }
    
    func index(at: Int) -> String.Index {
        return self.index(self.startIndex, offsetBy: at)
    }
    
    func localized() ->String {
        let lang = UserDefaults.standard.value(forKey: "currentAppLanguage") as! String
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: self, comment: "")
    }
    
    public func checkValide(text:String,validation:String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: validation).inverted
        let inputString = text.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return text == filtered
    }
    
    public func isValidName() -> Bool {
        let emailRegEx = "^[A-Za-z]+$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public func isValidNameWithSpace() -> Bool {
        let emailRegEx = "^[A-Z a-z]+$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self.TrimString())
        return result
    }
    public func isValidPhoneWithPlus(phoneNumber:String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return phoneNumber == filtered
    }
    
    public func isValidPhoneWithOutPlus(phoneNumber: String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return phoneNumber == filtered
    }
    
    public func isValidAlpaNumeric(phoneNumber: String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return phoneNumber == filtered
    }
    
    public func isValidCardNumberWithSpace(phoneNumber: String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: "0123456789 ").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return phoneNumber == filtered
    }
    
    public func isValidCardThrugh(phoneNumber: String) -> Bool {
        let charcterSet = NSCharacterSet(charactersIn: "0123456789/").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return phoneNumber == filtered
    }
    
    
    public func isPasswordSame(password:String,confirmPassword:String) -> Bool {
        if password == confirmPassword{
            return true
        } else {
            return false
        }
    }
    
    public func isPwdLenth(password:String,confirmPassword:String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        } else {
            return false
        }
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension DateFormatter{
    convenience init(_ dateFormat:String){
        self.init()
        self.locale = Locale.init(identifier: "en_US_POSIX")
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
}

struct appFont {
    static let shared = appFont()
    func customLightSize(_ size:CGFloat) -> UIFont {
        return UIFont.init(name: "GillSans-Light", size: size)!
    }
    func customRegularSize(_ size:CGFloat) -> UIFont {
        return UIFont.init(name: "GillSans", size: size)!
    }
    func customBoldSize(_ size:CGFloat) -> UIFont {
        return UIFont.init(name: "GillSans-Bold", size: size)!
    }
    func customSemiBoldSize(_ size:CGFloat) -> UIFont {
        return UIFont.init(name: "GillSans-SemiBold", size: size)!
    }
    func customRegularBrandFree(_ size:CGFloat) -> UIFont  {
        return UIFont.init(name: "GillSans", size: size)!
    }
}

extension UIViewController : MBProgressHUDDelegate, ReachableDelegate {
    func reachabilityChange() {
        ///
    }
    
    
    func hideKeyboardWhenTappedAround(){
        
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(){
         view.endEditing(true)
    }
    func showAlertMessage(message:String,title:String) -> Void {
        
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func ShowProgressHUD() {
        DispatchQueue.main.async{
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
        }
    }
    
    func HideProgressHUD() {
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view,animated:true)
        }
    }
    

    func getLanguage()->String
    {
        return UserDefaults.standard.value(forKey: "currentAppLanguage") as! String
    }
    
    /******************** custom  Pop up method ***********************************/
}
extension UIColor {
    convenience init(hexValue: String) {
        let hex = hexValue.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UINavigationController {
    func pop(animated: Bool) {
        _ = self.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
}

extension Date {
    
    static func daysBetweenDates(start: Date,endDate: Date) -> Int{
        let calendar: Calendar = Calendar.current
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: endDate)
        return calendar.dateComponents([.day], from: date1, to: date2).day!
    }
}

class DesignableView: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addInnerShadow()
    }
    var innerShadow = CALayer()
    var isInsert = false
    
    open var isInnerShadow = false {
        didSet
        {
            if isInnerShadow == false
            {
                innerShadow.shadowOpacity = 0.0
                self.layer.shadowOpacity = 0.7
                self.layer.masksToBounds = false
            }
            else
            {
                innerShadow.shadowOpacity = 1.0
                self.layer.shadowOpacity = 0.0
                self.layer.masksToBounds = true
                
                innerShadow.frame = bounds
                // Shadow path (1pt ring around bounds)
                let path = UIBezierPath(rect: innerShadow.bounds.insetBy(dx: -1, dy: -1))
                let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
                path.append(cutout)
                innerShadow.shadowPath = path.cgPath
            }
        }
    }
    
    open func setNewShadow() {
        innerShadow.shadowOffset = CGSize.init(width: 0, height: 2)
        innerShadow.shadowRadius = 3
    }
    
    open func setShadowlength() {
        innerShadow.shadowRadius = 2
        innerShadow.shadowColor = UIColor.darkGray.cgColor
    }
    
    func addInnerShadow() {
        innerShadow.frame = bounds
        // Shadow path (1pt ring around bounds)
        let path = UIBezierPath(rect: innerShadow.bounds.insetBy(dx: -1, dy: -1))
        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        // Shadow properties
        innerShadow.shadowColor = UIColor.black.cgColor // UIColor(red: 0.71, green: 0.77, blue: 0.81, alpha: 1.0).cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 0
        innerShadow.shadowRadius = 3
        // Add
        if isInsert == false
        {
            isInsert = true
            layer.addSublayer(innerShadow)
        }
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            return nil
        }
    }
    
    func circularImage(size: CGSize?) -> UIImage {
        let newSize = size ?? self.size
        
        let minEdge = min(newSize.height, newSize.width)
        let size = CGSize(width: minEdge, height: minEdge)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        self.draw(in: CGRect(origin: CGPoint.zero, size: size), blendMode: .copy, alpha: 1.0)
        
        context!.setBlendMode(.copy)
        context!.setFillColor(UIColor.clear.cgColor)
        
        let rectPath = UIBezierPath(rect: CGRect(origin: CGPoint.zero, size: size))
        let circlePath = UIBezierPath.init(ovalIn: CGRect(origin: CGPoint.zero, size: size))
        rectPath.append(circlePath)
        rectPath.usesEvenOddFillRule = true
        rectPath.fill()
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result!
    }
    
    func overlayWith(image: UIImage, posX: CGFloat, posY: CGFloat,size:CGSize = CGSize.init(width: 50, height: 71),newSize1:CGSize = CGSize.init(width: 36, height: 36)) -> UIImage {
        let newWidth = size.width < posX + newSize1.width ? posX + newSize1.width : size.width
        let newHeight = size.height < posY + newSize1.height ? posY + newSize1.height : size.height
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        image.draw(in: CGRect(origin: CGPoint(x: posX, y: posY), size: newSize1))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}


class DesignableButton: UIButton {
}

class DesignableLabel: UILabel {
}

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func shake(count : Float? = nil,for duration : TimeInterval? = nil,withTranslation translation : Float? = nil) {
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        animation.repeatCount = count ?? 2
        animation.duration = (duration ?? 0.5)/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.byValue = translation ?? -5
        layer.add(animation, forKey: "shake")
    }
    
    func shakeWithCompletion(count : Float? = nil,for duration : TimeInterval? = nil,withTranslation translation : Float? = nil,completion:@escaping ()->Void) {
        layer.removeAllAnimations()
        CATransaction.begin()
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        animation.repeatCount = count ?? 2
        animation.duration = (duration ?? 0.5)/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.byValue = translation ?? -5
        
        CATransaction.setCompletionBlock {
            completion()
        }
        
        layer.add(animation, forKey: "shake")
        
        CATransaction.commit()
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 3, clockwise: CGFloat = 1) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2) * clockwise
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func zoomIn(_ duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform.identity
        }) { (animationCompleted: Bool) -> Void in
        }
    }
   
    func zoomOut(_ duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0001, y: 0.0001)
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    func addDashedBorder() {
        let color = AppColor.navigationColor.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = (self.frame.size)
        let shapeRect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(rect: shapeRect).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}


extension UITextField{

    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
