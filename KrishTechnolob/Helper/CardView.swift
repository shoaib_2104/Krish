//
//  CardView.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//
import Foundation
import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.shadowRadius = newValue
            layer.masksToBounds = false
        }
    }
    
    @IBInspectable override var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        } set {
            layer.shadowOpacity = newValue
            layer.shadowColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBInspectable override var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        } set {
            layer.shadowOffset = newValue
            layer.shadowColor = UIColor.lightGray.cgColor
            layer.masksToBounds = false
        }
    }
    @IBInspectable
    override var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }

    @IBInspectable
         override var borderWidth: CGFloat {
            get {
                return layer.borderWidth
            }
            set {
                layer.borderWidth = newValue
            }
        }
}
