//
//  ProductFilterGroup.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductFilterGroup : NSObject, NSCoding{

    var filters : [ProductFilter]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        filters = [ProductFilter]()
        let filtersArray = json["filters"].arrayValue
        for filtersJson in filtersArray{
            let value = ProductFilter(fromJson: filtersJson)
            filters.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if filters != nil{
        var dictionaryElements = [[String:Any]]()
        for filtersElement in filters {
        	dictionaryElements.append(filtersElement.toDictionary())
        }
        dictionary["filters"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		filters = aDecoder.decodeObject(forKey: "filters") as? [ProductFilter]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if filters != nil{
			aCoder.encode(filters, forKey: "filters")
		}

	}

}
