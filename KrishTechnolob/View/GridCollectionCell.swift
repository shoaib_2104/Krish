//
//  GridCollectionCell.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit

class GridCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productImage: UIImageView!
}
