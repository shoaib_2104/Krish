//
//  ProductExtensionAttribute.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductExtensionAttribute : NSObject, NSCoding{

    var appendedData : ProductAppendedDatum!
    var categoryLinks : [ProductCategoryLink]!
    var productLabels : [String]!
    var websiteIds : [Int]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        let appendedDataJson = json["appended_data"]
        if !appendedDataJson.isEmpty{
            appendedData = ProductAppendedDatum(fromJson: appendedDataJson)
        }
        categoryLinks = [ProductCategoryLink]()
        let categoryLinksArray = json["category_links"].arrayValue
        for categoryLinksJson in categoryLinksArray{
            let value = ProductCategoryLink(fromJson: categoryLinksJson)
            categoryLinks.append(value)
        }
        productLabels = [String]()
        let productLabelsArray = json["product_labels"].arrayValue
        for productLabelsJson in productLabelsArray{
            productLabels.append(productLabelsJson.stringValue)
        }
        websiteIds = [Int]()
        let websiteIdsArray = json["website_ids"].arrayValue
        for websiteIdsJson in websiteIdsArray{
            websiteIds.append(websiteIdsJson.intValue)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if appendedData != nil{
        	dictionary["appendedData"] = appendedData.toDictionary()
        }
        if categoryLinks != nil{
        var dictionaryElements = [[String:Any]]()
        for categoryLinksElement in categoryLinks {
        	dictionaryElements.append(categoryLinksElement.toDictionary())
        }
        dictionary["categoryLinks"] = dictionaryElements
        }
        if productLabels != nil{
        	dictionary["product_labels"] = productLabels
        }
        if websiteIds != nil{
        	dictionary["website_ids"] = websiteIds
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		appendedData = aDecoder.decodeObject(forKey: "appended_data") as? ProductAppendedDatum
		categoryLinks = aDecoder.decodeObject(forKey: "category_links") as? [ProductCategoryLink]
		productLabels = aDecoder.decodeObject(forKey: "product_labels") as? [String]
		websiteIds = aDecoder.decodeObject(forKey: "website_ids") as? [Int]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if appendedData != nil{
			aCoder.encode(appendedData, forKey: "appended_data")
		}
		if categoryLinks != nil{
			aCoder.encode(categoryLinks, forKey: "category_links")
		}
		if productLabels != nil{
			aCoder.encode(productLabels, forKey: "product_labels")
		}
		if websiteIds != nil{
			aCoder.encode(websiteIds, forKey: "website_ids")
		}

	}

}
