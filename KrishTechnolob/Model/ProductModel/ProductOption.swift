//
//  ProductOption.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductOption : NSObject, NSCoding{

    var count : String!
    var id : String!
    var label : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        count = json["count"].stringValue
        id = json["id"].stringValue
        label = json["label"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if count != nil{
        	dictionary["count"] = count
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if label != nil{
        	dictionary["label"] = label
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		count = aDecoder.decodeObject(forKey: "count") as? String
		id = aDecoder.decodeObject(forKey: "id") as? String
		label = aDecoder.decodeObject(forKey: "label") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if label != nil{
			aCoder.encode(label, forKey: "label")
		}

	}

}
