//
//  ProductCategoryLink.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductCategoryLink : NSObject, NSCoding{

    var categoryId : String!
    var position : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        categoryId = json["category_id"].stringValue
        position = json["position"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if categoryId != nil{
        	dictionary["category_id"] = categoryId
        }
        if position != nil{
        	dictionary["position"] = position
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
		position = aDecoder.decodeObject(forKey: "position") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if categoryId != nil{
			aCoder.encode(categoryId, forKey: "category_id")
		}
		if position != nil{
			aCoder.encode(position, forKey: "position")
		}

	}

}
