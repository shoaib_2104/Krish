//
//  ProductModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductModel : NSObject, NSCoding{

    var direction : [String]!
    var filters : [ProductFilter]!
    var items : [ProductItem]!
    var searchCriteria : ProductSearchCriterion!
    var sortOrders : [ProductSortOrder]!
    var totalCount : Int!
    var wishlistIds : [Int]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        direction = [String]()
        let directionArray = json["direction"].arrayValue
        for directionJson in directionArray{
            direction.append(directionJson.stringValue)
        }
        filters = [ProductFilter]()
        let filtersArray = json["filters"].arrayValue
        for filtersJson in filtersArray{
            let value = ProductFilter(fromJson: filtersJson)
            filters.append(value)
        }
        items = [ProductItem]()
        let itemsArray = json["items"].arrayValue
        for itemsJson in itemsArray{
            let value = ProductItem(fromJson: itemsJson)
            items.append(value)
        }
        let searchCriteriaJson = json["search_criteria"]
        if !searchCriteriaJson.isEmpty{
            searchCriteria = ProductSearchCriterion(fromJson: searchCriteriaJson)
        }
        sortOrders = [ProductSortOrder]()
        let sortOrdersArray = json["sort_orders"].arrayValue
        for sortOrdersJson in sortOrdersArray{
            let value = ProductSortOrder(fromJson: sortOrdersJson)
            sortOrders.append(value)
        }
        totalCount = json["total_count"].intValue
        wishlistIds = [Int]()
        let wishlistIdsArray = json["wishlist_ids"].arrayValue
        for wishlistIdsJson in wishlistIdsArray{
            wishlistIds.append(wishlistIdsJson.intValue)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if direction != nil{
        	dictionary["direction"] = direction
        }
        if filters != nil{
        var dictionaryElements = [[String:Any]]()
        for filtersElement in filters {
        	dictionaryElements.append(filtersElement.toDictionary())
        }
        dictionary["filters"] = dictionaryElements
        }
        if items != nil{
        var dictionaryElements = [[String:Any]]()
        for itemsElement in items {
        	dictionaryElements.append(itemsElement.toDictionary())
        }
        dictionary["items"] = dictionaryElements
        }
        if searchCriteria != nil{
        	dictionary["searchCriteria"] = searchCriteria.toDictionary()
        }
        if sortOrders != nil{
        var dictionaryElements = [[String:Any]]()
        for sortOrdersElement in sortOrders {
        	dictionaryElements.append(sortOrdersElement.toDictionary())
        }
        dictionary["sortOrders"] = dictionaryElements
        }
        if totalCount != nil{
        	dictionary["total_count"] = totalCount
        }
        if wishlistIds != nil{
        	dictionary["wishlist_ids"] = wishlistIds
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		direction = aDecoder.decodeObject(forKey: "direction") as? [String]
		filters = aDecoder.decodeObject(forKey: "filters") as? [ProductFilter]
		items = aDecoder.decodeObject(forKey: "items") as? [ProductItem]
		searchCriteria = aDecoder.decodeObject(forKey: "search_criteria") as? ProductSearchCriterion
		sortOrders = aDecoder.decodeObject(forKey: "sort_orders") as? [ProductSortOrder]
		totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int
		wishlistIds = aDecoder.decodeObject(forKey: "wishlist_ids") as? [Int]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if direction != nil{
			aCoder.encode(direction, forKey: "direction")
		}
		if filters != nil{
			aCoder.encode(filters, forKey: "filters")
		}
		if items != nil{
			aCoder.encode(items, forKey: "items")
		}
		if searchCriteria != nil{
			aCoder.encode(searchCriteria, forKey: "search_criteria")
		}
		if sortOrders != nil{
			aCoder.encode(sortOrders, forKey: "sort_orders")
		}
		if totalCount != nil{
			aCoder.encode(totalCount, forKey: "total_count")
		}
		if wishlistIds != nil{
			aCoder.encode(wishlistIds, forKey: "wishlist_ids")
		}

	}

}
