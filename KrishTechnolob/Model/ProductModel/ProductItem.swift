//
//  ProductItem.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductItem : NSObject, NSCoding{

    var attributeSetId : Int!
    var createdAt : String!
    var customAttributes : [ProductCustomAttribute]!
    var extensionAttributes : ProductExtensionAttribute!
    var id : Int!
    var mediaGalleryEntries : [ProductMediaGalleryEntry]!
    var name : String!
    var options : [String]!
    var price : Int!
    var productLinks : [String]!
    var sku : String!
    var status : Int!
    var tierPrices : [String]!
    var typeId : String!
    var updatedAt : String!
    var visibility : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        attributeSetId = json["attribute_set_id"].intValue
        createdAt = json["created_at"].stringValue
        customAttributes = [ProductCustomAttribute]()
        let customAttributesArray = json["custom_attributes"].arrayValue
        for customAttributesJson in customAttributesArray{
            let value = ProductCustomAttribute(fromJson: customAttributesJson)
            customAttributes.append(value)
        }
        let extensionAttributesJson = json["extension_attributes"]
        if !extensionAttributesJson.isEmpty{
            extensionAttributes = ProductExtensionAttribute(fromJson: extensionAttributesJson)
        }
        id = json["id"].intValue
        mediaGalleryEntries = [ProductMediaGalleryEntry]()
        let mediaGalleryEntriesArray = json["media_gallery_entries"].arrayValue
        for mediaGalleryEntriesJson in mediaGalleryEntriesArray{
            let value = ProductMediaGalleryEntry(fromJson: mediaGalleryEntriesJson)
            mediaGalleryEntries.append(value)
        }
        name = json["name"].stringValue
        options = [String]()
        let optionsArray = json["options"].arrayValue
        for optionsJson in optionsArray{
            options.append(optionsJson.stringValue)
        }
        price = json["price"].intValue
        productLinks = [String]()
        let productLinksArray = json["product_links"].arrayValue
        for productLinksJson in productLinksArray{
            productLinks.append(productLinksJson.stringValue)
        }
        sku = json["sku"].stringValue
        status = json["status"].intValue
        tierPrices = [String]()
        let tierPricesArray = json["tier_prices"].arrayValue
        for tierPricesJson in tierPricesArray{
            tierPrices.append(tierPricesJson.stringValue)
        }
        typeId = json["type_id"].stringValue
        updatedAt = json["updated_at"].stringValue
        visibility = json["visibility"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if attributeSetId != nil{
        	dictionary["attribute_set_id"] = attributeSetId
        }
        if createdAt != nil{
        	dictionary["created_at"] = createdAt
        }
        if customAttributes != nil{
        var dictionaryElements = [[String:Any]]()
        for customAttributesElement in customAttributes {
        	dictionaryElements.append(customAttributesElement.toDictionary())
        }
        dictionary["customAttributes"] = dictionaryElements
        }
        if extensionAttributes != nil{
        	dictionary["extensionAttributes"] = extensionAttributes.toDictionary()
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if mediaGalleryEntries != nil{
        var dictionaryElements = [[String:Any]]()
        for mediaGalleryEntriesElement in mediaGalleryEntries {
        	dictionaryElements.append(mediaGalleryEntriesElement.toDictionary())
        }
        dictionary["mediaGalleryEntries"] = dictionaryElements
        }
        if name != nil{
        	dictionary["name"] = name
        }
        if options != nil{
        	dictionary["options"] = options
        }
        if price != nil{
        	dictionary["price"] = price
        }
        if productLinks != nil{
        	dictionary["product_links"] = productLinks
        }
        if sku != nil{
        	dictionary["sku"] = sku
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if tierPrices != nil{
        	dictionary["tier_prices"] = tierPrices
        }
        if typeId != nil{
        	dictionary["type_id"] = typeId
        }
        if updatedAt != nil{
        	dictionary["updated_at"] = updatedAt
        }
        if visibility != nil{
        	dictionary["visibility"] = visibility
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		attributeSetId = aDecoder.decodeObject(forKey: "attribute_set_id") as? Int
		createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
		customAttributes = aDecoder.decodeObject(forKey: "custom_attributes") as? [ProductCustomAttribute]
		extensionAttributes = aDecoder.decodeObject(forKey: "extension_attributes") as? ProductExtensionAttribute
		id = aDecoder.decodeObject(forKey: "id") as? Int
		mediaGalleryEntries = aDecoder.decodeObject(forKey: "media_gallery_entries") as? [ProductMediaGalleryEntry]
		name = aDecoder.decodeObject(forKey: "name") as? String
		options = aDecoder.decodeObject(forKey: "options") as? [String]
		price = aDecoder.decodeObject(forKey: "price") as? Int
		productLinks = aDecoder.decodeObject(forKey: "product_links") as? [String]
		sku = aDecoder.decodeObject(forKey: "sku") as? String
		status = aDecoder.decodeObject(forKey: "status") as? Int
		tierPrices = aDecoder.decodeObject(forKey: "tier_prices") as? [String]
		typeId = aDecoder.decodeObject(forKey: "type_id") as? String
		updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
		visibility = aDecoder.decodeObject(forKey: "visibility") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if attributeSetId != nil{
			aCoder.encode(attributeSetId, forKey: "attribute_set_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if customAttributes != nil{
			aCoder.encode(customAttributes, forKey: "custom_attributes")
		}
		if extensionAttributes != nil{
			aCoder.encode(extensionAttributes, forKey: "extension_attributes")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if mediaGalleryEntries != nil{
			aCoder.encode(mediaGalleryEntries, forKey: "media_gallery_entries")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if options != nil{
			aCoder.encode(options, forKey: "options")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productLinks != nil{
			aCoder.encode(productLinks, forKey: "product_links")
		}
		if sku != nil{
			aCoder.encode(sku, forKey: "sku")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if tierPrices != nil{
			aCoder.encode(tierPrices, forKey: "tier_prices")
		}
		if typeId != nil{
			aCoder.encode(typeId, forKey: "type_id")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if visibility != nil{
			aCoder.encode(visibility, forKey: "visibility")
		}

	}

}
