//
//  ListTableCell.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit

class ListTableCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
