//
//  GridViewController.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit
import Reachability


class GridViewController: UIViewController {
    
    static func storyboardInstance() -> GridViewController? {
        let Storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return Storyboard.instantiateViewController(withIdentifier: "GridViewController") as? GridViewController
    }
    
    @IBOutlet weak var gridCollection: UICollectionView!
    var delegate:ListDelegate?
    private let spacing:CGFloat = 15.0
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var totalPages : Int = 10
    var productMdl : ProductModel!
    var productItems = [ProductItem]()
    var reachability = Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gridCollection.delegate = self
        gridCollection.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        self.gridCollection?.collectionViewLayout = layout
        self.getProductsList(isFirst: true, currentPage: self.currentPage)
    
    }
    
    func getProductsList(isFirst:Bool, currentPage:Int){
        let url:String = "https://mcstaging.tamata.com/rest/V1/mdcategories/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=467&searchCriteria[filter_groups][0][filters][0][condition_type]=in&storeId=2&currencyCode=IQD&searchCriteria[pageSize]=10&categoryId=26&searchCriteria[currentPage]=\(currentPage)"
        if reachability?.connection == .wifi || reachability?.connection == .cellular {
            APIServices.gertUrlSession2(urlString: url, params: [:]) { (response) in
                
                if response.json.exists() {
                    self.productMdl = ProductModel.init(fromJson: response.json)
                    if isFirst{
                        self.productItems = self.productMdl.items
                    }else{
                        self.productItems += self.productMdl.items
                    }
                    self.gridCollection.reloadData()
                }
            }
           
        } else {
            BaseClass.sharedInstance.showNotificationBanner(
                AppTitleForNotificationBanner,
                ErrorInternetConnection,
                .Error
            )
        }
       self.HideProgressHUD()
    }
}
extension GridViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = gridCollection.dequeueReusableCell(withReuseIdentifier: "GridCollectionCell", for: indexPath) as! GridCollectionCell
        cell.name.text = self.productItems[indexPath.row].name
        
        let price = "\(self.productItems[indexPath.row].price ?? 10)"
        cell.productPrice.text = price
        let apdURL = self.productItems[indexPath.row].extensionAttributes.appendedData.mediaPath ?? ""
        if self.productItems[indexPath.row].mediaGalleryEntries.count > 0{
            let imgURl = self.productItems[indexPath.row].mediaGalleryEntries[0].file ?? ""
            print(imgURl)
            let fullUrl = apdURL+imgURl
            cell.productImage.sd_setImage(with: URL(string: fullUrl), placeholderImage: #imageLiteral(resourceName: "krish"))
        }else{
            cell.productImage.image = #imageLiteral(resourceName: "krish")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.currentPage < self.totalPages{
            if indexPath.row == (self.productItems.count-1){
                //Call API
                self.currentPage = self.currentPage + 1
                self.getProductsList(isFirst: false, currentPage: self.currentPage)
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 16
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        if let collection = self.gridCollection{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: 200)
        } else {
            return CGSize(width: 0, height: 200)
        }
    }
}
