//
//  ProductCustomAttribute.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductCustomAttribute : NSObject, NSCoding{

    var attributeCode : String!
    var value : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        attributeCode = json["attribute_code"].stringValue
        value = json["value"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if attributeCode != nil{
        	dictionary["attribute_code"] = attributeCode
        }
        if value != nil{
        	dictionary["value"] = value
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		attributeCode = aDecoder.decodeObject(forKey: "attribute_code") as? String
		value = aDecoder.decodeObject(forKey: "value") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if attributeCode != nil{
			aCoder.encode(attributeCode, forKey: "attribute_code")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}
