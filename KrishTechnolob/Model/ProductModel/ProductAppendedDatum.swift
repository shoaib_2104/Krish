//
//  ProductAppendedDatum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductAppendedDatum : NSObject, NSCoding{

    var additionalInformation : [String]!
    var defaultVendorData : [ProductDefaultVendorDatum]!
    var mediaPath : String!
    var otherVendorData : AnyObject!
    var ratingSummary : Int!
    var reviewCount : Int!
    var ruleRelatedProducts : AnyObject!
    var vendorLogoPath : String!
    var wishlistFlag : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        additionalInformation = [String]()
        let additionalInformationArray = json["additional_information"].arrayValue
        for additionalInformationJson in additionalInformationArray{
            additionalInformation.append(additionalInformationJson.stringValue)
        }
        defaultVendorData = [ProductDefaultVendorDatum]()
        let defaultVendorDataArray = json["default_vendor_data"].arrayValue
        for defaultVendorDataJson in defaultVendorDataArray{
            let value = ProductDefaultVendorDatum(fromJson: defaultVendorDataJson)
            defaultVendorData.append(value)
        }
        mediaPath = json["media_path"].stringValue
        otherVendorData = json["other_vendor_data"] as AnyObject
        ratingSummary = json["rating_summary"].intValue
        reviewCount = json["review_count"].intValue
        ruleRelatedProducts = json["rule_related_products"] as AnyObject
        vendorLogoPath = json["vendor_logo_path"].stringValue
        wishlistFlag = json["wishlist_flag"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if additionalInformation != nil{
        	dictionary["additional_information"] = additionalInformation
        }
        if defaultVendorData != nil{
        var dictionaryElements = [[String:Any]]()
        for defaultVendorDataElement in defaultVendorData {
        	dictionaryElements.append(defaultVendorDataElement.toDictionary())
        }
        dictionary["defaultVendorData"] = dictionaryElements
        }
        if mediaPath != nil{
        	dictionary["media_path"] = mediaPath
        }
        if otherVendorData != nil{
        	dictionary["other_vendor_data"] = otherVendorData
        }
        if ratingSummary != nil{
        	dictionary["rating_summary"] = ratingSummary
        }
        if reviewCount != nil{
        	dictionary["review_count"] = reviewCount
        }
        if ruleRelatedProducts != nil{
        	dictionary["rule_related_products"] = ruleRelatedProducts
        }
        if vendorLogoPath != nil{
        	dictionary["vendor_logo_path"] = vendorLogoPath
        }
        if wishlistFlag != nil{
        	dictionary["wishlist_flag"] = wishlistFlag
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		additionalInformation = aDecoder.decodeObject(forKey: "additional_information") as? [String]
		defaultVendorData = aDecoder.decodeObject(forKey: "default_vendor_data") as? [ProductDefaultVendorDatum]
		mediaPath = aDecoder.decodeObject(forKey: "media_path") as? String
		otherVendorData = aDecoder.decodeObject(forKey: "other_vendor_data") as? AnyObject
		ratingSummary = aDecoder.decodeObject(forKey: "rating_summary") as? Int
		reviewCount = aDecoder.decodeObject(forKey: "review_count") as? Int
		ruleRelatedProducts = aDecoder.decodeObject(forKey: "rule_related_products") as? AnyObject
		vendorLogoPath = aDecoder.decodeObject(forKey: "vendor_logo_path") as? String
		wishlistFlag = aDecoder.decodeObject(forKey: "wishlist_flag") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if additionalInformation != nil{
			aCoder.encode(additionalInformation, forKey: "additional_information")
		}
		if defaultVendorData != nil{
			aCoder.encode(defaultVendorData, forKey: "default_vendor_data")
		}
		if mediaPath != nil{
			aCoder.encode(mediaPath, forKey: "media_path")
		}
		if otherVendorData != nil{
			aCoder.encode(otherVendorData, forKey: "other_vendor_data")
		}
		if ratingSummary != nil{
			aCoder.encode(ratingSummary, forKey: "rating_summary")
		}
		if reviewCount != nil{
			aCoder.encode(reviewCount, forKey: "review_count")
		}
		if ruleRelatedProducts != nil{
			aCoder.encode(ruleRelatedProducts, forKey: "rule_related_products")
		}
		if vendorLogoPath != nil{
			aCoder.encode(vendorLogoPath, forKey: "vendor_logo_path")
		}
		if wishlistFlag != nil{
			aCoder.encode(wishlistFlag, forKey: "wishlist_flag")
		}

	}

}
