//
//  ProductMediaGalleryEntry.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductMediaGalleryEntry : NSObject, NSCoding{

    var disabled : Bool!
    var file : String!
    var id : Int!
    var label : String!
    var mediaType : String!
    var position : Int!
    var types : [String]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        disabled = json["disabled"].boolValue
        file = json["file"].stringValue
        id = json["id"].intValue
        label = json["label"].stringValue
        mediaType = json["media_type"].stringValue
        position = json["position"].intValue
        types = [String]()
        let typesArray = json["types"].arrayValue
        for typesJson in typesArray{
            types.append(typesJson.stringValue)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if disabled != nil{
        	dictionary["disabled"] = disabled
        }
        if file != nil{
        	dictionary["file"] = file
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if label != nil{
        	dictionary["label"] = label
        }
        if mediaType != nil{
        	dictionary["media_type"] = mediaType
        }
        if position != nil{
        	dictionary["position"] = position
        }
        if types != nil{
        	dictionary["types"] = types
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		disabled = aDecoder.decodeObject(forKey: "disabled") as? Bool
		file = aDecoder.decodeObject(forKey: "file") as? String
		id = aDecoder.decodeObject(forKey: "id") as? Int
		label = aDecoder.decodeObject(forKey: "label") as? String
		mediaType = aDecoder.decodeObject(forKey: "media_type") as? String
		position = aDecoder.decodeObject(forKey: "position") as? Int
		types = aDecoder.decodeObject(forKey: "types") as? [String]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if disabled != nil{
			aCoder.encode(disabled, forKey: "disabled")
		}
		if file != nil{
			aCoder.encode(file, forKey: "file")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if label != nil{
			aCoder.encode(label, forKey: "label")
		}
		if mediaType != nil{
			aCoder.encode(mediaType, forKey: "media_type")
		}
		if position != nil{
			aCoder.encode(position, forKey: "position")
		}
		if types != nil{
			aCoder.encode(types, forKey: "types")
		}

	}

}
