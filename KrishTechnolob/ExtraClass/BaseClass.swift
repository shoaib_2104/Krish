//
//  BaseClass.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import Foundation
import UIKit
import Reachability
import BRYXBanner


struct AppColor {
    static let navigationColor:UIColor = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    static let yellowColor:UIColor = UIColor.init(red: 251/255.0, green: 204/255.0, blue: 52/255.0, alpha: 1.0)
    static let greenColor:UIColor = UIColor.init(red: 129/255.0, green: 194/255.0, blue: 65/255.0, alpha: 1.0)
    static let textFeildBorderColor:UIColor = UIColor.darkGray
    static let errorColor:UIColor = UIColor.red
    static let textPlaceHolderColor = UIColor.init(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0)
    static let textPlaceLableColor = UIColor.darkGray
    static let notificationBannerError:UIColor = UIColor.init(red: 255/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
    static let notificationBannerGreen:UIColor = UIColor.init(red: 46/255.0, green: 152/255.0, blue: 0/255.0, alpha: 1.0)
    static let notificationBannerWarning:UIColor = UIColor.init(red: 230/255.0, green: 165/255.0, blue: 65/255.0, alpha: 1.0)
}

enum BannerType:String {
    case Success = "1"
    case Error = "2"
    case Warning = "3"
}

class BaseClass: NSObject {
    public class var sharedInstance: BaseClass {
        struct Singleton {
            static let instance = BaseClass()
        }
        return Singleton.instance
    }
    var reachability = Reachability()
    
    func showErrorMessage(_ error:Error) {
        if error.localizedDescription != "The Internet connection appears to be offline." && error.localizedDescription != "The network connection was lost." {
            self.showNotificationBanner(
                AppTitleForNotificationBanner,
                error.localizedDescription,
                .Error)
        }
    }
    
    func showNotificationBanner(_ strTitle:String,_ strMessage:String,_ type:BannerType) {
        if type == .Error {
            let banner = Banner(title: strTitle, subtitle: strMessage, image: nil, backgroundColor: AppColor.notificationBannerError)
            banner.dismissesOnTap = true
            banner.springiness = .none
            banner.show(duration: 2.0)
        } else if type == .Success {
            let banner = Banner(title: strTitle, subtitle: strMessage, image: nil, backgroundColor: AppColor.notificationBannerGreen)
            banner.dismissesOnTap = true
            banner.springiness = .none
            banner.show(duration: 2.0)
        } else {
            let banner = Banner(title: strTitle, subtitle: strMessage, image: nil, backgroundColor: AppColor.notificationBannerWarning)
            banner.dismissesOnTap = true
            banner.springiness = .none
            banner.show(duration: 2.0)
        }
    }
    
    func setShadowOnView(_ view:UIView,_ shadowSize:CGFloat,_ shadowColor:UIColor = UIColor.init(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0))  {
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: +shadowSize,
                                                   width: view.frame.size.width + shadowSize,
                                                   height: view.frame.size.height + shadowSize))
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowPath.cgPath
    }
    
    func setShadowAllSideOnView(_ view:UIView,_ shadowSize:CGFloat,_ shadowColor:UIColor = UIColor.init(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0))  {
        view.layer.shadowPath = nil
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: view.frame.size.width + shadowSize,
                                                   height: view.frame.size.height + shadowSize))
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowPath.cgPath
    }
    
    func setShadowOnButton(_ view:UIButton,_ shadowSize:CGFloat){
        let shadowColor = UIColor.init(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
        let shadowCorner = UIBezierPath.init(roundedRect: CGRect(x: -shadowSize / 2,
                                                                 y: 0,
                                                                 width: view.frame.size.width + shadowSize,
                                                                 height: view.frame.size.height + shadowSize), cornerRadius: view.layer.cornerRadius)
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowCorner.cgPath
    }
    
    func flipImageVertically(originalImage:UIImage) -> UIImage {
        let image:UIImage = UIImage.init(cgImage: originalImage.cgImage!, scale: 1.0, orientation: UIImage.Orientation.down)
        return image
    }
    
    func flipImageHorizontally(originalImage:UIImage) -> UIImage {
        let image:UIImage = UIImage.init(cgImage: originalImage.cgImage!, scale: 1.0, orientation: UIImage.Orientation.upMirrored)
        return image
    }

    func setStatusBar(_ view:UIView, color:UIColor = AppColor.navigationColor) {
        let sView = UIView.init()
        sView.frame = CGRect.init(x: 0, y: 0, width: view.frame.width, height: UIApplication.shared.statusBarFrame.height)
        sView.backgroundColor = color
        view.addSubview(sView)
    }
    
}
