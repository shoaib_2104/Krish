//
//  HomeViewController.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 14/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit
import PageMenu

protocol ListDelegate {
    func goListView()
    func goGridView()
}

@available(iOS 13.0, *)
class HomeViewController: UIViewController,CAPSPageMenuDelegate {
    static func storyboardInstance() -> HomeViewController? {
        let Storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return Storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
    }
    @IBOutlet weak var ContentView: UIView!
    var pageMenu:CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var currentPage = 0
    var insertPageMenu:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        if insertPageMenu == false{
            insertPageMenu = true
            self.perform(#selector(ConfigureMenu), with: nil, afterDelay: 0.0)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @objc func ConfigureMenu(){
        controllerArray.removeAll()
        let MenuCotrollerObj1 = ListViewController.storyboardInstance()
        MenuCotrollerObj1?.title = "LIST"
        let MenuCotrollerObj2 = GridViewController.storyboardInstance()
        MenuCotrollerObj2?.delegate = self
        MenuCotrollerObj2!.title = "GRID"
        controllerArray.append(MenuCotrollerObj1!)
        controllerArray.append(MenuCotrollerObj2!)
    
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0.0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.0),
            .scrollMenuBackgroundColor(UIColor.white),
            .menuHeight(50),
            .selectionIndicatorColor(.blue),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: ContentView.bounds, pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.view.backgroundColor = UIColor.clear
        ContentView.addSubview((pageMenu?.view)!)
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        if  index  == 0 {
            
        } else {
            
        }
    }
}
@available(iOS 13.0, *)
extension HomeViewController:ListDelegate {
    func goListView() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
        self.navigationController?.pushViewController(secondVC, animated: false)
    }
    
    func goGridView() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "GridViewController") as! GridViewController
        self.navigationController?.pushViewController(secondVC, animated: false)
    }
    
    
}


