//
//  ErrorView.swift
//  KrishTechnolob
//
//  Created by Vahida Sheikh on 13/04/20.
//  Copyright © 2020 shoaib sheikh. All rights reserved.
//

import UIKit

class ErrorView: UIView {

    private var lblMessage:UILabel!
    private var imgIcon:UIImageView!
    
    var labelFont:UIFont! {
        didSet {
            lblMessage.font = labelFont
        }
    }
    
    var labelColor:UIColor! {
        didSet {
            lblMessage.textColor = labelColor
        }
    }
    
    var message:String = "" {
        didSet {
            lblMessage.text = message
            self.perform(#selector(setHightValue), with: nil, afterDelay: 0.01)
        }
    }
    
    @objc func setHightValue() {
        let size = CGSize.init(width: lblMessage.frame.width, height: .infinity)
        let estimatedSize = lblMessage.sizeThatFits(size)
        heightValue = estimatedSize.height
        if heightValue < errorViewFixHeight {
            heightValue = errorViewFixHeight
        }
        for item in self.constraints {
            if item.firstAttribute == .height {
                item.constant = heightValue
            }
        }
    }
    
    var iconImage:UIImage? = nil {
        didSet {
            imgIcon.image = iconImage
        }
    }
    
    var heightValue:CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        
        lblMessage = UILabel.init()
        lblMessage.translatesAutoresizingMaskIntoConstraints = false
        lblMessage.lineBreakMode = .byTruncatingTail
        lblMessage.numberOfLines = 0
        lblMessage.textColor = UIColor.red
        lblMessage.font = appFont.shared.customRegularSize(17)
        self.addSubview(lblMessage)
        
        imgIcon = UIImageView.init()
        imgIcon.image = #imageLiteral(resourceName: "ic_error")
        imgIcon.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imgIcon)
        
        imgIcon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        imgIcon.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        imgIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        imgIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        lblMessage.leadingAnchor.constraint(equalTo: self.imgIcon.trailingAnchor, constant: 0).isActive = true
        lblMessage.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        lblMessage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        lblMessage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 8).isActive = true
    }
}
