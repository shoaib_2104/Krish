//
//  ProductSortOrder.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 14, 2020

import Foundation
import SwiftyJSON


class ProductSortOrder : NSObject, NSCoding{

    var key : String!
    var label : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        key = json["key"].stringValue
        label = json["label"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if key != nil{
        	dictionary["key"] = key
        }
        if label != nil{
        	dictionary["label"] = label
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		key = aDecoder.decodeObject(forKey: "key") as? String
		label = aDecoder.decodeObject(forKey: "label") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if key != nil{
			aCoder.encode(key, forKey: "key")
		}
		if label != nil{
			aCoder.encode(label, forKey: "label")
		}

	}

}
